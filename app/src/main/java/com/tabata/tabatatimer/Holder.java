package com.tabata.tabatatimer;

import android.annotation.SuppressLint;

import java.io.Serializable;

class Holder implements Serializable {

    private int iterationTotal;
    private int iterationLeft;

    private TabataModel tabata;
    private TabataModel currentTabata;

    Holder(int work, int relax, int iteration) {
        this.iterationTotal = iteration;
        this.iterationLeft = iteration;
        tabata = new TabataModel(work, relax);
        currentTabata = TabataModel.cloneInstance(tabata);
    }

    boolean isWorkActive() {
        return currentTabata.getWork() > 0;
    }

    boolean isRelaxActive() {
        return currentTabata.getRelax() > 0;
    }

    void decreaseWork() {
        currentTabata.decreaseWork();
    }

    void decreaseRelax() {
        currentTabata.decreaseRelax();
    }

    boolean nextIteration() {
        iterationLeft--;
        if (iterationLeft > 0) {
            currentTabata = TabataModel.cloneInstance(tabata);
            return true;
        } else {
            return false;
        }
    }

    int getWork() {
        return currentTabata.getWork();
    }

    int getRelax() {
        return currentTabata.getRelax();
    }

    @SuppressLint("DefaultLocale")
    String getIteration() {
        return String.format("%d/%d", iterationLeft, iterationTotal);
    }

    @SuppressLint("DefaultLocale")
    String getFinalIteration() {
        return String.format("0/%d", iterationTotal);
    }

}
