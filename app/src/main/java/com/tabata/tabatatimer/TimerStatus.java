package com.tabata.tabatatimer;

import android.support.annotation.IntDef;

@IntDef
@interface TimerStatus {
    int STOPPED = 0;
    int PAUSED = 1;
    int WORKING = 2;

}