package com.tabata.tabatatimer;

import java.io.Serializable;

class TabataModel implements Serializable {

    static TabataModel cloneInstance(TabataModel model) {
        return new TabataModel(model.work, model.relax);
    }

    private int work;
    private int relax;

    TabataModel(int work, int relax) {
        this.work = work;
        this.relax = relax;
    }

    void decreaseWork() {
        --work;
    }

    void decreaseRelax() {
        --relax;
    }

    int getWork() {
        return work;
    }

    int getRelax() {
        return relax;
    }
}
