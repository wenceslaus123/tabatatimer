package com.tabata.tabatatimer;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

import static com.tabata.tabatatimer.TimerStatus.PAUSED;
import static com.tabata.tabatatimer.TimerStatus.STOPPED;
import static com.tabata.tabatatimer.TimerStatus.WORKING;

public class MainActivity extends AppCompatActivity implements TabataManager.OnTickListener {

    public static final String HOLDER = "holder";

    @BindView(R.id.timer)
    TextView timerView;
    @BindView(R.id.iteration)
    TextView iteration;
    @BindView(R.id.root)
    View root;

    TabataManager tabataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if (savedInstanceState != null && savedInstanceState.containsKey(HOLDER)) {
            tabataManager = new TabataManager((Holder) savedInstanceState.getSerializable(HOLDER), this);
            tabataManager.init();
        }
    }

    @OnClick(R.id.timer)
    void onClick() {
        if (tabataManager == null || tabataManager.getStatus() == STOPPED) {
            tabataManager = new TabataManager(5, 3, 3, this);
            tabataManager.start();


        } else if (tabataManager.getStatus() == PAUSED) {
            tabataManager.start();


        } else if (tabataManager.getStatus() == WORKING) {
            tabataManager.pause();



        } else {
            throw new IllegalArgumentException("Unknown status");
        }
    }

    @OnLongClick(R.id.timer)
    boolean onLongClick() {
        tabataManager.stop();
        return true;
    }


    @Override
    public void tickWork(int tick, String it) {
        setBackground(Color.RED);
        timerView.setText(String.valueOf(tick));
        iteration.setText(it);
    }

    @Override
    public void tickRelax(int tick, String it) {
        setBackground(Color.GREEN);
        timerView.setText(String.valueOf(tick));
        iteration.setText(it);
    }

    @Override
    public void end(String it) {
        setBackground(Color.BLACK);
        timerView.setText(String.valueOf(0));
        iteration.setText(it);
    }

    private void setBackground(int color) {
        root.setBackgroundColor(color);
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(color);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (tabataManager != null && tabataManager.getStatus() == WORKING) {
            tabataManager.pause();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (tabataManager != null && tabataManager.getStatus() != STOPPED) {
            outState.putSerializable(HOLDER, tabataManager.getHolder());
        }
    }
}
