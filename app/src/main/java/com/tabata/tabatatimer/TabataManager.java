package com.tabata.tabatatimer;

import android.os.Handler;
import android.os.Message;

import static com.tabata.tabatatimer.TimerStatus.PAUSED;
import static com.tabata.tabatatimer.TimerStatus.STOPPED;
import static com.tabata.tabatatimer.TimerStatus.WORKING;

/**
 * Manage tabata counter
 */
class TabataManager {

    private Holder holder;
    private OnTickListener listener;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            handler.sendEmptyMessageDelayed(1, 1000);
            tick();
        }
    };

    @TimerStatus
    private int status = STOPPED;

    TabataManager(int work, int relax, int iteration, OnTickListener listener) {
        this.listener = listener;
        holder = new Holder(work, relax, iteration);
    }

    TabataManager(Holder holder, OnTickListener listener) {
        this.listener = listener;
        this.holder = holder;
        this.status = PAUSED;
    }

    private void tick() {
        if (holder.isWorkActive()) {
            listener.tickWork(holder.getWork(), holder.getIteration());
            holder.decreaseWork();



        } else if (holder.isRelaxActive()) {
            listener.tickRelax(holder.getRelax(), holder.getIteration());
            holder.decreaseRelax();



        } else if (holder.nextIteration()) {
            listener.tickWork(holder.getWork(), holder.getIteration());
            holder.decreaseWork();



        } else {
            stop();
        }
    }

    @TimerStatus
    int getStatus() {
        return status;
    }

    void pause() {
        handler.removeMessages(1);
        status = PAUSED;
    }

    void start() {
        handler.sendEmptyMessageDelayed(1, status == PAUSED ? 1000 : 0);
        status = WORKING;
    }

    void stop() {
        handler.removeMessages(1);
        status = STOPPED;
        listener.end(holder.getFinalIteration());
    }

    public void init() {
        if (holder.isWorkActive()) {
            listener.tickWork(holder.getWork() + 1, holder.getIteration());
        } else if (holder.isRelaxActive()) {
            listener.tickRelax(holder.getRelax() + 1, holder.getIteration());
        }
    }

    interface OnTickListener {
        void tickWork(int tick, String iteration);

        void tickRelax(int tick, String iteration);

        void end(String iteration);
    }

    public Holder getHolder() {
        return holder;
    }


}
